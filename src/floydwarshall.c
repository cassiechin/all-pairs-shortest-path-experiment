/**
 * Cassie Chin (cassiechin9793@gmail.com)
 * https://bitbucket.org/cassiechin/csce4110-term-project
 *
 * CSCE 4110, Project
 *
 * Floyd-Warshall uses dynamic programming to solve the All Pairs 
 * Shortest Path problem.
 * 
 * This program takes in an input file representing the graph and 
 * outputs the results to a file
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define FALSE 0
#define TRUE 1
#define BUFFER_SIZE 10000 // size of a single line in a file
#define INDEX_TRIAL 1 // argv index with number of trials
#define INDEX_INPUT 2 // argv index of input file
#define INDEX_OUTPUT 3 // argv index of output file
#define INDEX_TIME 4 // argv index of time file
#define INDEX_TOTAL 5 // number of argv parameters

// Variable names are maintain from the sudo code located at www.cse.unt.edu/~4110s001/dijk.pdf
int n; // number of vertices
int** A; // cost array [fromVertex][toVertex]

/**
 * Converts a string representing an integer, to an integer type
 * 
 * @param string String to convert
 * @return the integer
 */
int strToInt (char *string) {
	return (int)strtol(string, (char **)NULL, 10);
}

/**
 * Initializes the cost array
 * 
 * @param numVertices The number of vertices the cost array has to support
 */
void initGlobalVariables(int numVertices) {
	// The number of vertices
	n = numVertices;

	// A 2D array represent the directed cost between adjacent vertices
	A = (int **) calloc (n, sizeof(int*));
	int i=0;
	for (i=0; i<n; i++) A[i] = (int *) calloc (n, sizeof(int)); 
}

/**
 * Frees the global cost array
 */
void freeGlobalVariables() {
	int i;
	for (i=0; i<n; i++) free (A[i]);
	free (A);
}

/**
 * Takes the data from a line in an input file and adds it to the cost array
 * 
 * @param buffer The string containing the distances to add to the cost array
 * @param fromVertex The row of the cost matrix to edit
 */
void processLine (char *buffer, int fromVertex) {
	int toVertex = 0;
	char *token = strtok(buffer, " ");
	while (token != NULL) {
		int weight = strToInt(token);
		A[fromVertex][toVertex++] = weight;
		token = strtok (NULL, " ");
	}
}

/**
 * Print the cost array to a file
 * 
 * @param fp The file to print the distances to
 */
void printDistances (FILE *fp) {
	int i,j;
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			fprintf (fp, "%d ", A[i][j]);
		}
		fprintf(fp,"\n");
	}
}

/**
 * Print the cost array to the terminal
 */
void printDistances_terminal () {
	int i,j;
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			printf ("%d ", A[i][j]);
		}
		printf("\n");
	}
}

/**
 * Function to find the minimum of two integers
 * 
 * @param A the first integer
 * @param B the second integer
 * @return The smaller of the two integers, returns B if they are the same
 */
int minimum (int A, int B) {
	if (A < B) return A;
	else return B;
}

/**
 * Run Floyd-Warshall based on what is currently in the cost array
 */
void floydwarshall () {
	int k,i,j;
	for (k=0; k<n; k++)
		for (i=0; i<n; i++)
			for (j=0; j<n; j++)
				A[i][j] = minimum (A[i][j], A[i][k] + A[k][j]);
}

/**
 * Reads in the input file, and runs floyd warshall
 * 
 * @param argc The number of command line arguments
 * @param argv[1] The graph input file
 * @param argv[2] The distances output file
 */
void main (int argc, char* argv[]) {
	if (argc != INDEX_TOTAL) return;  
	int num_trials = strToInt (argv[INDEX_TRIAL]); 
	FILE *f_in = fopen(argv[INDEX_INPUT], "r");
	FILE *f_out = fopen(argv[INDEX_OUTPUT], "w");
	FILE *f_time = fopen(argv[INDEX_TIME], "a");
	if (!f_in || !f_out || !f_time) return;
  
	// Process the input file
	char *buffer = (char *) calloc (BUFFER_SIZE, sizeof(char));
	int vertex_num = -1; // The first line is the number of vertices, not distances
	while (fgets (buffer, BUFFER_SIZE, f_in)) {
		buffer[strlen(buffer)-1] = '\0';
		if (vertex_num == -1) { // This is the first line, so don't call processLine
			initGlobalVariables(strToInt(buffer));
			vertex_num++;
		}
		else {
			processLine(buffer, vertex_num++);
		}
	}
	  
	// Run Floyd-Warshall
	int trial;
	clock_t begin = clock();
	for (trial=0; trial<num_trials; trial++) floydwarshall();
	clock_t end = clock();

	// Save results
	printDistances (f_out);
	fprintf(f_time, "%d, %lf\n", n, (((double)(end - begin)) / CLOCKS_PER_SEC)/num_trials);
	//printf("d, %d, %lf\n", n, (((double)(end - begin)) / CLOCKS_PER_SEC)/num_trials);
  
	// Clean up and return
	freeGlobalVariables();
	fclose(f_in);
	fclose(f_out);
	fclose(f_time);

	return;
} 
