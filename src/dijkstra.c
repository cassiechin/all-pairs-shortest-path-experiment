/**
 * Cassie Chin (cassiechin9793@gmail.com)
 * https://bitbucket.org/cassiechin/csce4110-term-project
 *
 * All Pairs Shortest Path Using Dijkstra's Algorithm
 *
 * Dijkstra's algorithm finds the shortest path for a vertex to all other vertices.
 * Running this algorithm on every vertex, will give the shortest path between all 
 * combinations of source to destination vertices.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define FALSE 0
#define TRUE 1
#define INDEX_TRIAL 1 // argv index with number of trials
#define INDEX_INPUT 2 // argv index of input file
#define INDEX_OUTPUT 3 // argv index of output file
#define INDEX_TIME 4 // argv index of time file
#define INDEX_TOTAL 5 // number of argv parameters

// Length of a line in the input data file
#define BUFFER_SIZE 10000

// Variable names are maintain from the sudo code located at www.cse.unt.edu/~4110s001/dijk.pdf
int n; // number of vertices
int* S; // saves whether a vertex has been visted, has to be cleared for each new "fromVertex"  
int** dist; // distance array [fromVertex][toVertex]
int** cost; // cost array [fromVertex][toVertex]

/**
 * Convert a string of an integer to an int variable
 * @param string The string to convert
 * @return the string integer
 */
int strToInt (char *string) {
	return (int)strtol(string, (char **)NULL, 10);
}

/**
 * Allocate space for the S, dist, and cost arrays
 * @param numVertices The number of vertices to perform dijkstra's on 
 */
void initGlobalVariables(int numVertices) {
	// The number of vertices
	n = numVertices; 

	// A 1D array representing whether a node has been visited
	S = (int *) calloc (n, sizeof(int));

	// A 2D array representing the shortest paths between [fromVertex][toVertex]
	dist = (int **) calloc (n, sizeof(int*));

	// A 2D array represent the directed cost between adjacent vertices
	cost = (int **) calloc (n, sizeof(int*));

	int i=0;
	for (i=0; i<n; i++) {
		dist[i] = (int *) calloc (n, sizeof(int));
		cost[i] = (int *) calloc (n, sizeof(int)); 
	}
}

/**
 * Free's the dist, cost, and S array
 */
void freeGlobalVariables() {
	int i;
	for (i=0; i<n; i++) {
		free (dist[i]);
		free (cost[i]);
	}

	free (dist);
	free (cost);
	free (S);
}

/**
 * Takes in a string buffer representing one line of the input file.
 * Parses the integers in that line and stores it in the correct spot in the cost matrix.
 * Also initializes the distance matrix at the same time.
 * The initial values in the distance matrix are the same as the cost matrix
 *
 * @param fromVertex Says what row of the cost matrix to store the values in
 */
void processLine (char *buffer, int fromVertex) {
	int toVertex = 0;
	char *token = strtok(buffer, " ");
	while (token != NULL) {
		int weight = strToInt(token);
		cost[fromVertex][toVertex] = weight;
		dist[fromVertex][toVertex++] = weight;
		token = strtok (NULL, " ");
	}
}

/**
 * Finds the next available vertex with the smallest distance
 * 
 * @param v The vertex row to look at in the distance matrix
 * @return The next availbe vertex with the smallest distance
 */
int chooseNextVertex (int v) {
	int minDistValue = 999999;
	int minDistIndex = -1;

	int i;
	for (i=0; i<n; i++) {
		// Ignore vertices that have already been visited
		if (S[i] == TRUE) continue;

		// The current value for distance is the smallest so far
		if (dist[v][i] < minDistValue) {
			minDistValue = dist[v][i];
			minDistIndex = i;
		}
	}

	return minDistIndex;
}

/**
 * Prints the distance matrix out to a file
 *
 * @param fp The file to print to
 */
void printDistances (FILE *fp) {
	int i,j;
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			fprintf (fp, "%d ", dist[i][j]);
		}
		fprintf(fp,"\n");
	}
}

/**
 * Performs dijkstra's algorithm
 *
 * @param v The starting vertex
 */
void dijkstra (int v) {
	// Mark the first vertex as visited
	S[v] = TRUE;

	// Determine n-1 paths from v
	while (1) {
		// Choose u from among those vertices not in S such that dist[u] is minimum
		int u = chooseNextVertex(v);

		// There are no more vertices to choose from
		if (u == -1) break;

		// Marks the chosen vertex as visited
		S[u] = TRUE;

		// For each w adjacent to u, that have not been visted, update the distances
		int w;
		for (w=0; w<n; w++) {
			if (S[w] == FALSE) {
				// Update distances
				// The distance to w is greater than the the distance to u plus the cost of going from u to w
				if (dist[v][w] > dist[v][u] + cost[u][w]) {
					dist[v][w] = dist[v][u] + cost[u][w];
				}
			}
		}
	}

	// Reset the visited array
	int i;
	for (i=0; i<n; i++) S[i] = FALSE;
}

/**
 * Read input from the file
 * Perform all pairs shortest path for multiple trials
 * Save distance results
 * Save time results
 * 
 * @param argv[1] The name of the input data file
 */
void main (int argc, char* argv[]) {
	// Process command line arguments
	if (argc != INDEX_TOTAL) return; // Return if there aren't enough
	int num_trials = strToInt(argv[INDEX_TRIAL]); // store the number of trials 
	FILE *f_in = fopen(argv[INDEX_INPUT], "r"); // open the input file
	FILE *f_out = fopen(argv[INDEX_OUTPUT], "w"); // open the output file
	FILE *f_time = fopen(argv[INDEX_TIME], "a"); // open the time file
	if (!f_in || !f_out || !f_time) return; // return on file error
    
	// Process the input file
	char *buffer = (char *) calloc (BUFFER_SIZE, sizeof(char));
	int vertex_num = -1; // The first line is the number of vertices, not distances
	while (fgets (buffer, BUFFER_SIZE, f_in)) {
		buffer[strlen(buffer)-1] = '\0';
		if (vertex_num == -1) { // This is the first line, so don't call processLine
			initGlobalVariables(strToInt(buffer));
			vertex_num++;
		}
		else {
			processLine(buffer, vertex_num++);
		}
	}

	// Run the all pairs shortest path algorithm multiple times
	int trial, i;
	clock_t begin = clock();
	for (trial=0; trial<num_trials; trial++) {
		for (i=0; i<n; i++) dijkstra (i); // All pairs shortest path means dijkstra's has to run on every vertex
	}
	clock_t end = clock();

	// Save results
	printDistances (f_out);
	fprintf(f_time, "%d, %lf\n", n, (((double)(end - begin)) / CLOCKS_PER_SEC)/num_trials);
	//printf("d, %d, %lf\n", n, (((double)(end - begin)) / CLOCKS_PER_SEC)/num_trials);
  
	// Clean up and return
	freeGlobalVariables();
	fclose(f_in);
	fclose(f_out);
	fclose(f_time);
  
	return;
} 
