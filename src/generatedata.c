/**
 * Cassie Chin
 * https://bitbucket.org/cassiechin/csce4110-term-project.git
 * cassiechin9793@gmail.com
 * 10/21/14
 * CSCE 4110, Project
 *
 * Graph Generator
 * This file generates the graphs that will be used as input to the Dijkstra and Floyd-Warshall Algorithm.
 * The output of the program will be stored in a folder as specified by the FOLDER constant.
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX_EDGE_VALUE 200 // The maximum value an edge can be
#define SIZE_FILENAME 256 // The maximum length of the filename
#define INDEX_MODE 1 // The index of the command line argument that says the graph mode
#define INDEX_VERTICES 2 // The index of the command line argument that says the number of vertices

const char BASE_FOLDER[] = "data"; // The base folder for all the data
const char GRAPH_DATA_FOLDER[] = "data/input"; // The folder that the results will get stored to
const char DIJKSTRA_FOLDER[] = "data/d-output"; // The folder that the graph output will get stored to
const char FLOYDWARSHALL_FOLDER[] = "data/fw-output"; // The folder that floyd warshal will get stored to

// The mode that the graph will be generated in
#define MODE_RANDOM 1
#define MODE_SMART_RANDOM 2
#define MODE_INCREASING 3
#define MODE_DECREASING 4
#define MODE_CONSTANT 5

// Smart random constants
#define MAX_EDGE_TYPES 3
#define MAX_EDGE_HIGH MAX_EDGE_VALUE
#define MAX_EDGE_MIDDLE ((int)(MAX_EDGE_VALUE/2))
#define MAX_EDGE_LOW ((int)(MAX_EDGE_VALUE/4))

// Increasing/Decreasing Global Variable
int previousEdgeWeight; // Store the previous edge weight

/**
 * Returns a random number between 1 and the MAX_EDGE_VALUE
 */
int mode_random () {
	return ((rand() % MAX_EDGE_VALUE) + 1);
}

/**
 * An alternate version of random that should force more lower numbers.
 */
int mode_smart_random() {
	// Randomly select whether this will be a smaller or larger range
	int select = rand() % 3;
  
	int maxEdgeValue;
	// on select = 0, a smaller number can still be randomly chosen,
	// on select = 2, we know a small number will be randomly chosen
	if (select == 0) maxEdgeValue = MAX_EDGE_VALUE; 
	if (select == 1) maxEdgeValue = MAX_EDGE_VALUE/2; 
	if (select == 2) maxEdgeValue = MAX_EDGE_VALUE/4; 

	// generate the random number based on the bounds chosen
	return ((rand() % maxEdgeValue) + 1);	
}

/**
 * Every new edge value generated is higher than the previous
 */
int mode_increasing () {
	return previousEdgeWeight++;
}

/**
 *  Every new edge value generated is lower than the previous
 */
int mode_decreasing () {
	return previousEdgeWeight--;
}

/**
 * All edges are 1
 */
int mode_constant() {
	return 1;
}

/**
 * Returns an edge depending on a mode
 */
int generateEdgeValue (int mode) {
	switch (mode) {
		case MODE_RANDOM: return mode_random();
		case MODE_SMART_RANDOM: return mode_smart_random();
		case MODE_INCREASING: return mode_increasing();
		case MODE_DECREASING: return mode_decreasing();		
		case MODE_CONSTANT: return mode_constant();
		default: break;
	}
	return 0;
}

/**
 * Writes numbers to a file to represent a directed complete graph.
 * The first line of the file will represent the number of vertices.
 * The distance between a vertex and itself will always be 0.
 *
 * @param fp The file to write to
 * @param num_of_vertices The number of vertices that the graph will have
 */
void generateGraph (FILE *fp, int mode, int num_of_vertices) {
	int i, j, edge_value;

	if (mode == MODE_INCREASING) previousEdgeWeight = 1;	
	if (mode == MODE_DECREASING) previousEdgeWeight = num_of_vertices * num_of_vertices;

	// The first line of the file will be the number of vertices
	fprintf(fp, "%d\n", num_of_vertices);

	for (i=0; i<num_of_vertices; i++) {            // represents the from vertex
		for (j=0; j<num_of_vertices; j++) {          // represents the to vertex
			if (i == j) edge_value = 0;                // the distance from a vertex to itself is 0
			else edge_value = generateEdgeValue(mode); // randomize an edge value
			fprintf (fp, "%d ", edge_value);           // write the edge value to the file
		}
		fprintf (fp, "\n");
	}	 
}

/**
 * returns the a filename for the graph to be stored in
 *
 * @param num_of_vertices This number is used along with timestamp to create the filename
 */
char *generateFilename(int mode, int num_of_vertices) {
	char *filepath= (char*) calloc (SIZE_FILENAME, sizeof(char));
	sprintf(filepath, "%s/%d_%3d_%u.txt", GRAPH_DATA_FOLDER, mode, num_of_vertices, (unsigned)time(NULL)); 
	return filepath;
}

/**
 * Reads the command line arguments and creates a new file for each one.
 */
int main (int argc, char* argv[]) {
	srand(time(NULL));

	// Create the folder if it doesn't exist
	mkdir (BASE_FOLDER, 0777);
	mkdir (GRAPH_DATA_FOLDER, 0777);
	mkdir (DIJKSTRA_FOLDER, 0777);
	mkdir (FLOYDWARSHALL_FOLDER, 0777);

	// Get the mode number
	int mode = (int) strtol(argv[INDEX_MODE], (char **)NULL, 10);

	// The command line string needs to be converted to an integer
	int num_of_vertices = (int) strtol(argv[INDEX_VERTICES], (char **)NULL, 10);

	// Create the filename using the number of vertices the graph will have
	char *filename = generateFilename(mode, num_of_vertices);

	// Generate the graph and write the results to the file
	FILE *fp = fopen (filename, "w");    
	if (fp) {
		generateGraph (fp, mode, num_of_vertices);
		//printf("Generated: [%s]\n", filename);
		fclose(fp);
	} 
	// If there was an error creating the file, then let the user know
	else {
		printf("File Error: [%s]\n", filename);
	}
  
	return 0; 
} 
