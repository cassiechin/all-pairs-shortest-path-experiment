# README #

### Who do I talk to? ###
* Cassie Chin
* cassiechin9793@gmail.com
* [Website](http://cassiechin.com)
* [Repository](https://bitbucket.org/cassiechin/csce4110-term-project)

### What is this repository for? ###

* CSCE 4110
* [Project Requirements](http://www.cse.unt.edu/~4110s001/project.pdf)
* Solve the All Pairs Shortest Path Problem using Dijkstra's and Floyd-Warshall

**Note: All commands in this README should be executed from the base folder**

### Parts of this Repository ###
+ docs/
	* Contains reports and project requirements
+ src/
	* Contains all the C source code
+ exe/
	* Contains all scripts and binary executables
+ data/
	* Contains all input and output data
	* This folder is automatically generated when `./run-project` or `./exe/run-g <mode>` is called

### How do I compile the project? ###
+ `./exe/compile`
+ This command will generate binary executables in the exe folder
	* `exe/run-g` for generatedata.c
	* `exe/run-d` for dijkstra.c
	* `exe/run-fw` for floyd-warshall.c

### How do I clean out data? ###
* `./exe/clean`

### How do I run the whole project in one command? ###
* `./run-project`
* You will be prompted for the graph generation mode
* You will be prompted for the number of trials to run each file for each different all pairs shortest path method
* The program input distances are stored in the data/input/ folder
* The program output distances can be viewed in the data/d-output/ and data/fw-output/ folders
* The time statistics are appended to the end of data/time_d.txt for dijkstra or data/time_fw.txt for floyd-warshall


### How do I generate graphs? ###
* `./exe/compile`
* `./exe/run-g [MODE_NUMBER] [NUM_VERTICES]`

### How do I run only Dijkstra's? ###
* `./exe/compile`
* `./exe/run-d [NUM_TRIALS] [INPUT_FILE] [OUTPUT_FILE] [TIME_FILE]`

### How do I run only Floyd-Warshall? ###
* `./exe/compile`
* `./exe/run-fw [NUM_TRIALS] [INPUT_FILE] [OUTPUT_FILE] [TIME_FILE]`

### How do I use my own input files? ###
* The first line in the file should contain a single number representing the number of vertices
* Each subsequent line represents edge distances
* The second line in the file represents edge distances from the first node
* The last line in the file represents edge distances from the last node
* On each line each distance is separated by a single space
* The distance between a vertex and itself is assumed to be zero when the program reads the file
* The program assumes correct file input format, if the input file is not formatted correctly, the program will not execute correctly
